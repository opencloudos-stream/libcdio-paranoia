Summary: CD paranoia on top of libcdio
Name: libcdio-paranoia
Version: 10.2+2.0.1
Release: 6%{?dist}
License: GPLv3+
URL: http://www.gnu.org/software/libcdio/
Source0: http://ftp.gnu.org/gnu/libcdio/libcdio-paranoia-%{version}.tar.bz2
Patch3000: libcdio-paranoia-manpage.patch

BuildRequires: make gcc
BuildRequires: pkgconfig chrpath
BuildRequires: gettext-devel libcdio-devel

%description
This package provides a CDDA reader distribution ('libcdio-cdparanoia') that reads audio 
directly from the CDROM as data, with no analog step between, and writes the data to a file 
or pipe as .wav, .aifc or as raw 16 bit linear PCM.

%package devel
Summary: Header files and libraries for %{name}
Requires: %{name} = %{version}-%{release}

%description devel
This package provides header files and libraries for %{name}.

%prep
%autosetup -p1

sed -i -e 's,-I${includedir},-I${includedir}/cdio,g' libcdio_paranoia.pc.in
sed -i -e 's,-I${includedir},-I${includedir}/cdio,g' libcdio_cdda.pc.in

f=doc/ja/cd-paranoia.1.in
iconv -f euc-jp -t utf-8 -o $f.utf8 $f && mv $f.utf8 $f
iconv -f ISO88591 -t utf-8 -o THANKS.utf8 THANKS && mv THANKS.utf8 THANKS

%build
%configure --disable-dependency-tracking --disable-static --disable-rpath
%make_build

%install
%make_install

find %{buildroot} -type f -name "*.la" -exec rm -f {} ';'

mv %{buildroot}%{_mandir}/{jp,ja}

cp -a %{buildroot}%{_includedir}/cdio/paranoia/*.h %{buildroot}%{_includedir}/cdio/

chrpath --delete %{buildroot}%{_bindir}/*
chrpath --delete %{buildroot}%{_libdir}/*.so.*

%check
%make_build check

%files
%license COPYING
%doc AUTHORS NEWS.md README.md THANKS
%{_bindir}/*
%{_libdir}/*.so.*
%{_mandir}/man1/*
%lang(ja) %{_mandir}/ja/man1/*

%files devel
%doc doc/overlapdef.txt
%{_includedir}/cdio/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc

%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 10.2+2.0.1-6
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 10.2+2.0.1-5
- Rebuilt for loongarch release

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 10.2+2.0.1-4
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 10.2+2.0.1-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 10.2+2.0.1-2
- Rebuilt for OpenCloudOS Stream 23

* Fri Dec 16 2022 Wang Guodong <gordonwwang@tencent.com> - 10.2+2.0.1-1
- package init.
